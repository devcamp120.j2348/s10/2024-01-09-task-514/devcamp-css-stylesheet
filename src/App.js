import avatar from './assets/images/avatar.jpg';
import './App.css';

function App() {
  return (
    <div className="dc-container">
      <img src={avatar} className='dc-image' />
      <p className='dc-quote'>This is one of the best developer blogs on the planet! I read it daily to improve my skills</p>
      <p><b>Tammy Stevens</b> Front End Developer</p>
    </div>
  );
}

export default App;
